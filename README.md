IASD-Bootstrap
==============

Para utilizar o IASD Boostrap, leia a documentação disponibilizada em [http://styleguide.adventistas.org](http://styleguide.adventistas.org)

Como gerar a documentação para publicação
-----------------------------------------

No diretório raiz do projeto

	:::shell
	$ npm install
	$ bundle install
	$ grunt docs
	$ bundle exec jekyll build

Após estes comandos um diretório chamado __gh_pages será criado.

Para publicar a documentação, é necessário ter a ferramenta s3cmd instalada e configurada com as chaves de acesso corretas para o bucket do styleguide na Amazon S3 (no exemplo abaixo as configurações estão definidas no arquivo ~/.s3cfg-styleguide-iasd).

	:::shell
	$ s3cmd -c ~/.s3cfg-styleguide-iasd sync --delete-removed --acl-public _gh_pages/ s3://styleguide.adventistas.org

Observações: `--acl-public` é obrigatório. trailingslash no diretório `_gh_pages/` também é obrigatório.


Como publicar a gema
--------------------

Apenas usuário com permissões específicas conseguirão publicar a gema.

No diretório raiz do projeto

	:::shell
	# 1. Gere a Gema
	$ npm install
	$ grunt dist-gem
	$ gem build iasd-bootstrap-sass.gemspec
	
	# 2. Instale a gema e faça testes da gema em um diretório vazio
	$ gem install iasd-bootstrap-sass-<version>.gem
	$ mkdir /tmp/test
	$ cd /tmp/test
	$ compass config
	$ echo "require 'iasd-bootstrap-sass'" >> config/compass.rb
	$ compass install iasd-bootstrap
	$ cd -

	# 3. Publique a gem
	$ gem push iasd-bootstrap-sass-<version>.gem

	# 4. Teste a gema publicada. Remova a gema gerada e instale a publicada
	$ gem uninstall iasd-bootstrap-sass -v <version>
	$ gem install iasd-bootstrap-sass -v <version>

Como gerar a versão compilada para distribuição
-----------------------------------------------

No diretório raiz do projeto

	:::shell
	$ npm install
	$ grunt dist-zip

Isto deve compilar os estilos e gerar um arquivo .zip pronto para distribuição

