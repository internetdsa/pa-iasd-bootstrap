(function($){

	$(document).ready(function (){
		InstitucionalController.initProgressionPlayer();
	});
	

	$(window).resize(function(){
		if ($('audio.progression-single').size()){
			//there's a calculation bug on resize. reload fixes it temporarely
			window.location.reload();
		}		
	});


	var InstitucionalController = {

		initProgressionPlayer : function(){
			$('audio.progression-single').mediaelementplayer({
		    	audioHeight: 40, // height of audio player
		    	startVolume: 1, // initial volume when the player starts
		    	features: ['playpause','current','progress','duration','tracks','volume','fullscreen']
		    });
		}
	};


})(jQuery);	
