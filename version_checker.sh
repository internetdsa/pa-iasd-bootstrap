#!/bin/bash

ESC_SEQ="\x1b["
EXPECTED_VERSION=v$(cat lib/iasd-bootstrap-sass/version.rb|grep 'VERSION' | sed 's/[^"]*"\([^"]*\).*/\1/')

STATICS="http://styleguide.adventistas.org/static
http://www.adventistas.org/pt/wp-content/themes/pa-thema-sedes/static
http://www.adventistas.org/es/wp-content/themes/pa-thema-sedes/static
http://noticias.adventistas.org/pt/wp-content/themes/pa-thema-asn/static
http://noticias.adventistas.org/es/wp-content/themes/pa-thema-asn/static"

STATICS_GZIPED="
http://blogs.adventistas.org/pt/wp-content/themes/pa-thema-blogs/static
http://blogs.adventistas.org/es/wp-content/themes/pa-thema-blogs/static
http://downloads.adventistas.org/pt/wp-content/themes/pa-thema-videos/static
http://downloads.adventistas.org/es/wp-content/themes/pa-thema-videos/static
http://videos.adventistas.org/pt/wp-content/themes/pa-thema-videos/static
http://videos.adventistas.org/es/wp-content/themes/pa-thema-videos/static
http://usb.adventistas.org/wp-content/themes/pa-thema-sedes/static"

for i in $STATICS; do
	SITE=$(echo $i | sed 's/\(.*adventistas\.org\/[^\/]*\/\).*/\1/')
	VERSION=$(curl -s $i/lib/iasd-bootstrap.min.js | grep 'IASD Bootstrap' | sed 's/.*\(v[0-9][^ ]*\).*/\1/')
	
	if [ $VERSION = $EXPECTED_VERSION ]; then
		echo -e "${SITE} ${ESC_SEQ}30;42;1m ${VERSION} ${ESC_SEQ}0m"
	else 
		echo -e "${SITE} ${ESC_SEQ}37;41;1m ${VERSION} ${ESC_SEQ}0m"
	fi

done

for i in $STATICS_GZIPED; do
	SITE=$(echo $i | sed 's/\(.*adventistas\.org\/[^\/]*\/\).*/\1/')
	VERSION=$(curl -s $i/lib/iasd-bootstrap.min.js | gunzip | grep 'IASD Bootstrap' | sed 's/.*\(v[0-9][^ ]*\).*/\1/')
	if [ $VERSION = $EXPECTED_VERSION ]; then
		echo -e "${SITE} ${ESC_SEQ}30;42;1m ${VERSION} ${ESC_SEQ}0m"
	else 
		echo -e "${SITE} ${ESC_SEQ}37;41;1m ${VERSION} ${ESC_SEQ}0m"
	fi

done

# 
